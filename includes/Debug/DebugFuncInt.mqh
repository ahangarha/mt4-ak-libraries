//+------------------------------------------------------------------+
//|                                                Debug/FuncInt.mqh |
//|                                                   Alasdair Keyes |
//|                https://gitlab.com/alasdairkeyes/mt4-ak-libraries |
//|                                                                  |
//| See LICENSE file for License                                     |
//| See README.md file for usage                                     |
//|                                                                  |
//| This header file is for internal use only.                       |
//| Including it in your EAs won't help                              |
//+------------------------------------------------------------------+

/*
   Print output to Console based on provided debug level
*/
void Debug(int MessageLevel, string DebugMessage) {
   if (DebugLevel >= MessageLevel)
      Print(DebugMessage);
}
