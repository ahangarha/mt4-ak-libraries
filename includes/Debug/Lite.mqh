//+------------------------------------------------------------------+
//|                                                   Debug/Lite.mqh |
//|                                                   Alasdair Keyes |
//|                https://gitlab.com/alasdairkeyes/mt4-ak-libraries |
//|                                                                  |
//| See LICENSE file for License                                     |
//| See README.md file for usage                                     |
//|                                                                  |
//| Provide Basic debugging levels to your MQL EAs                   |
//+------------------------------------------------------------------+
#property copyright "Alasdair Keyes"
#property link      "https://gitlab.com/alasdairkeyes/mt4-ak-libraries"
#property strict
#include "DebugFuncInt.mqh"

// Define Custom Debug Constants
// Lite style
#define DEBUG_OFF       0
#define DEBUG_BASIC     10
#define DEBUG_FULL      20

// This enum maps the standardish Debug levels as per Syslog
enum DebugValues {
   Off   = DEBUG_OFF,
   Basic = DEBUG_BASIC,
   Full  = DEBUG_FULL,
};

// Define the input variable that will be used
input DebugValues  DebugLevel = DEBUG_OFF;
