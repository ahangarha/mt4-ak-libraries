//+------------------------------------------------------------------+
//|                                                   Debug/Full.mqh |
//|                                                   Alasdair Keyes |
//|                https://gitlab.com/alasdairkeyes/mt4-ak-libraries |
//|                                                                  |
//| See LICENSE file for License                                     |
//| See README.md file for usage                                     |
//|                                                                  |
//| Provide Syslog style debugging to your MQL EAs                   |
//+------------------------------------------------------------------+
#property copyright "Alasdair Keyes"
#property link      "https://gitlab.com/alasdairkeyes/mt4-ak-libraries"
#property strict
#include "DebugFuncInt.mqh"

// Define Custom Debug Constants
#define DEBUG_OFF       0
#define DEBUG_DEBUG     10
#define DEBUG_INFO      20
#define DEBUG_NOTICE    30
#define DEBUG_WARNING   40
#define DEBUG_ERROR     50
#define DEBUG_CRITICAL  60
#define DEBUG_ALERT     70
#define DEBUG_EMERGENCY 80

// This enum maps the standardish Debug levels as per Syslog
enum DebugValues {
   Off         = DEBUG_OFF,
   Debug       = DEBUG_DEBUG,
   Info        = DEBUG_INFO,
   Notice      = DEBUG_NOTICE,
   Warning     = DEBUG_WARNING,
   Error       = DEBUG_ERROR,
   Critical    = DEBUG_CRITICAL,
   Alert       = DEBUG_ALERT,
   Emergency   = DEBUG_EMERGENCY
};

// Define the input variable that will be used
input DebugValues  DebugLevel = DEBUG_OFF;
