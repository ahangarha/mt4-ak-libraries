# mt4-ak-libraries

Collection of Libraries to extend MQL4 functionality

## Debug

These allow you do provide Output to the Experts log with varying degrees of importance

### Debug/Full.mqh

Add logging based on the Syslog levels of `DEBUG`, `INFO`, `NOTICE`, `WARNING`, `ERROR`, `CRITICAL`, `ALERT`, `EMERGENCY`.

### Import

Import this header with the following Line.

```
#include "path/to/Debug/Full.mqh"
```

This imports the following into your script's namespace

#### Constants

* `DEBUG_OFF`
* `DEBUG_DEBUG`
* `DEBUG_INFO`
* `DEBUG_NOTICE`
* `DEBUG_WARNING`
* `DEBUG_ERROR`
* `DEBUG_CRITICAL`
* `DEBUG_ALERT`
* `DEBUG_EMERGENCY`

#### Enumerators

* `DebugValues`

#### Variables

* `DebugValues` `DebugLevel`

#### EA Input variables

* `DebugLevel` - Inserts enum drop-box to select the debug level. Default is `DEBUG_OFF`

#### Functions

```
Debug(
    int     MessageLevel,   // The Debug level of this message
    string  DebugMessage    // The message to output
);
```

### Example

```
Debug(
  DEBUG_INFO,
  "This is an INFO warning, it will display if DebugLevel is set to INFO or higher"
);

int SomeInt = 3;
Debug(
  DEBUG_CRITICAL,
  StringFormat(
    "This is a Critical Debug to report the value of SomeInt=%d", SomeInt)
);
```

### Debug/Lite.mqh

Add logging based on the Syslog levels of `BASIC`, `FULL`.

### Import

Import this header with the following Line.

```
#include "path/to/Debug/Lite.mqh"
```

This imports the following into your script's namespace

#### Constants

* `DEBUG_OFF`
* `DEBUG_BASIC`
* `DEBUG_FULL`

#### Enumerators

* `DebugValues`

#### Variables

* `DebugValues` `DebugLevel`

#### EA Input variables

* `DebugLevel` - Inserts enum drop-box to select the debug level. Default is `DEBUG_OFF`

#### Functions

```
Debug(
    int     MessageLevel,   // The Debug level of this message
    string  DebugMessage    // The message to output
);
```

### Example

```
Debug(
  DEBUG_BASIC,
  "This is an DEBUG_BASIC warning, it will display if DebugLevel is set to DEBUG_BASIC or higher"
);

int SomeInt = 3;
Debug(
  DEBUG_FULL,
  StringFormat(
    "This is a DEBUG_FULL message to report the value of SomeInt=%d", SomeInt)
);
```
